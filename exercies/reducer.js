const reducer = (state = [], action) => {
	const { payload } = action

	switch(action.type) {
		case 'ADD_EXPENSE':
			return [...state, payload]
		case 'UPDATE_EXPENSE':
			return state.map(s => {
				if(s.uuid === payload.uuid) {
					return payload
				} else {
					return s
				}
			})
		case 'DELETE_EXPENSE':
			return state.filter(s => s.uuid !== payload.uuid)
		default:
			return state
	}
}