
function addMessageToQueue(queue, text, date) {
  const message = {
    date,
    text,
  }
  const result = [...queue, message]
  return result
}

const queue = []
addMessageToQueue(queue, 'test', new Date())