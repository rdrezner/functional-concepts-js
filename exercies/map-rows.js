const mapRows = (results, mapper) => {
  const rows = (results && results.rows) || []
  return rows.map(mapper)
}
