const curry = (f) => (b) => (c) => f(b, c)

function curryT(f) {
  return function(b) {
    return function(c) {
      f(b, c)
    }
  }
}

curry((a,b)=>a+b)(1)(2)

const uncurry = f => (x,y) => f(x)(y)
const sum = (a) => (b) => a+b

uncurry(sum)(1,2)