const compose = (f, g) => (x) => f(g(x))

console.log(compose(a=>a+1, a=>a+2)(0)) //3